import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) // private readonly authService: AuthService,
  {}

  // @Post('auth/login')
  // login(@Body() req) {
  //   return this.authService.login(req.user);
  // }
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
