import { of } from 'rxjs';
import { Product } from './product.model';

export class ProductsFakeService {
  private products: Product[] = [
    {
      id: '1',
      title: 'first title',
      description: 'First product Description',
      price: 100,
    },
    {
      id: '2',
      title: 'second title',
      description: 'Second product Description',
      price: 200,
    },
    {
      id: '3',
      title: 'third title',
      description: 'Third product Description',
      price: 300,
    },
  ];
  getProducts = jest.fn().mockImplementation(() => of(this.products));
  public async findById(): Promise<void> {}
}
