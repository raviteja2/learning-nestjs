import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { ProductType, ProductDocument, ProductSchema } from './product.schema';
import { ProductsService } from './products.service';
import { Product } from './product.model';

describe('Products Service', () => {
  let productsService: ProductsService;
  let mockProductModel: Model<ProductDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          'mongodb+srv://raviteja:vishnu123$@sample-cluster.dquap.mongodb.net/sample_db?retryWrites=true&w=majority',
        ),
        MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
      ],

      providers: [
        ProductsService,
        // Mocking the mongoDB service
        {
          provide: getModelToken(ProductType.name),
          useValue: Model, // useValue should be Model to mock the implementations of the MongoDB functions
        },
      ],
    }).compile();

    productsService = module.get<ProductsService>(ProductsService);
    mockProductModel = module.get<Model<ProductDocument>>(
      getModelToken(ProductType.name),
    );
  });

  it('should be defined', () => {
    expect(productsService).toBeDefined();
  });

  it('findProduct should return the product', async () => {
    const product: Product = {
      id: '1',
      title: 'first title',
      description: 'first desc',
      price: 100,
    };
    const mockProductModelFindByIdSpy = jest
      .spyOn(mockProductModel, 'findById')
      .mockImplementation(() => {
        return {
          exec: jest.fn().mockResolvedValue(product as Product),
        } as any;
      });

    const foundProduct = await productsService.findProduct('1');
    // console.log(foundProduct);
    expect(mockProductModelFindByIdSpy).toBeCalled();
    expect(foundProduct).toEqual({
      id: '1',
      title: 'first title',
      description: 'first desc',
      price: 100,
    });
  });

  it('Insert a product on successful returns a new product title', async () => {
    const product: Product = {
      id: '1',
      title: 'first title',
      description: 'first desc',
      price: 100,
    };

    const mockProductModelSaveSpy = jest
      .spyOn(mockProductModel.prototype, 'save')
      .mockImplementation(() => Promise.resolve(product));
    const newProductTitle = await productsService.insertProduct(
      product.title,
      product.description,
      product.price,
    );
    console.log(newProductTitle);
    expect(newProductTitle).toBe('first title');
    expect(mockProductModelSaveSpy).toBeCalled();
  });
});
