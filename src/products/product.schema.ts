import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ProductDocument = ProductType & Document;

@Schema()
export class ProductType {
  @Prop()
  id: string;
  @Prop({ required: true })
  description: string;
  @Prop({ required: true })
  title: string;
  @Prop({ required: true })
  price: number;

  @Prop()
  image: string;
}

export const ProductSchema = SchemaFactory.createForClass(ProductType);
