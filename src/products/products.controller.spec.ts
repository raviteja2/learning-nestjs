import { Test } from '@nestjs/testing';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { Model } from 'mongoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { ProductType, ProductDocument, ProductSchema } from './product.schema';
import { ProductsFakeService } from './producFakeService';

describe('ProductsController', () => {
  let productsController: ProductsController;
  let productsService: ProductsService;
  let mockProductModel: Model<ProductDocument>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          'mongodb+srv://raviteja:vishnu123$@sample-cluster.dquap.mongodb.net/sample_db?retryWrites=true&w=majority',
        ),
        MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
      ],
      controllers: [ProductsController],
      providers: [
        {
          provide: ProductsService,
          useClass: ProductsFakeService,
        },
        {
          provide: getModelToken(ProductType.name),
          useValue: Model,
        },
      ],
    }).compile();

    productsService = moduleRef.get<ProductsService>(ProductsService);
    productsController = moduleRef.get<ProductsController>(ProductsController);
    mockProductModel = moduleRef.get<Model<ProductDocument>>(
      getModelToken(ProductType.name),
    );
  });

  describe('Product controller defined or not', () => {
    it('Product controller Defined', async () => {
      expect(productsController).toBeDefined();
    });
  });

  describe('Get all Products', () => {
    it('service called', async () => {
      productsController.getAllProducts();
      expect(await productsService.getProducts).toHaveBeenCalled();
    });
  });
});
