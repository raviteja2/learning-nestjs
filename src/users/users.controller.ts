import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Request,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { join } from 'path';
import path = require('path');
import { Observable, of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { UsersService } from './users.service';

// storage options
export const storage = {
  storage: diskStorage({
    destination: './uploads/profileimages',
    filename: (req, file, cb) => {
      const filename: string =
        path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
      const extension: string = path.parse(file.originalname).ext;

      cb(null, `${filename}${extension}`);
    },
  }),
};
@Controller('users')
export class UserController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  addUsers(
    @Body('email') email: string,
    @Body('password') password: string,
    @Body('username') username: string,
  ) {
    const user = this.usersService.addUsers(email, password, username);
    return user;
  }

  @Get()
  getAllUsers() {
    return this.usersService.getUsers();
  }
  // Uploading a single file
  @Post('upload')
  @UseInterceptors(FileInterceptor('file', storage))
  uploadFile(@UploadedFile() file): Observable<Object> {
    console.log(file);
    return of({ imagePath: file.filename });
  }

  @Get('profile-image/:imagename')
  findImage(
    @Param('imagename') imagename: string,
    @Res() res,
  ): Observable<Object> {
    return of(
      res.sendFile(join(process.cwd(), 'uploads/profileimages/' + imagename)),
    );
  }
}
