import { Injectable } from '@nestjs/common';
import { User } from './users.model';

@Injectable()
export class UsersService {
  private allUsers: User[] = [];
  addUsers(userEmail: string, userPassword: string, userName: string) {
    const userId = Math.floor(Math.random() * (10 - 1) + 1);
    const newUser = new User(userEmail, userPassword, userName, userId);
    this.allUsers.push(newUser);
    return newUser;
  }

  getUsers() {
    return [...this.allUsers];
  }
}
